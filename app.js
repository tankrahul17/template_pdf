var createError = require('http-errors');
var express = require('express');
var path = require('path');
var CONSTANT = require('./config/constant.json');
var database = require('./config/database'); 	// Get configuration file
var mongoose = require('mongoose');
var http = require('http');
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var config = require('./config/config');
var indexRouter = require('./routes/index');
const cors = require("cors");

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('superSecret', CONSTANT.superSecret);
app.set('port', process.env.PORT || config.api_server_port);

app.use(bodyParser.json());
app.use (bodyParser.urlencoded({extended:false}));
app.use(cors());


const multer = require('multer');

/***************************************** headers */

// Add headers

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header(
    'Access-Control-Allow-Methods',
    'GET,PUT,POST,DELETE,OPTIONS,PATCH'
  )
  res.header('Authorization', true)
  res.header('Access-Control-Allow-Credentials', true)
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  )
  next()
})


/***************************************** authentication */

// API ROUTES -------------------

// get an instance of the router for api routes
var apiRoutes = express.Router();

// route middleware to verify a token
apiRoutes.use(function (req, res, next) {

  next();
  // // check header or url parameters or post parameters for token
  // var token = req.body.token || req.query.token || req.headers.authorization;

  // // decode token
  // if (token) {
  //   token = token.split(" ")[1];
  //   // console.log('toke' + token + ' app.get() ' + app.get('superSecret'));
  //   // verifies secret and checks exp
  //   jwt.verify(token, app.get('superSecret'), function (err, decoded) {
  //     if (err) {
  //       return res.json({ success: 0, message: 'Failed to authenticate token.' });
  //     } else {
  //       // if everything is good, save to request for use in other routes
  //       req.decoded = decoded;
  //       next();
  //     }
  //   });

  // } else {

  //   // if there is no token
  //   // return an error
  //   return res.status(403).send({
  //     success: 0,
  //     message: 'No token provided.'
  //   });

  // }
});

/*-----------------------All Routes List---------------------------------*/

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'uploads');
  },
  filename: (req, file, cb) => {
    console.log(file);
    const filename = Date.now() + path.extname(file.originalname)
    req['body'][file.fieldname] = filename
    cb(null, filename);
  }
});
const fileFilter = (req, file, cb) => {
  if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
}
const upload = multer({ storage: storage, fileFilter: fileFilter });

/*----------------------------------------------------------------------*/


app.use('/api', apiRoutes);


/*-----------------------All Routes List---------------------------------*/
var templateroute = require('./routes/templateroute');

/*--------------------------- Template Routes------------------------------*/
app.get('/api/template/getTemplates', upload.none(), templateroute.getTemplates);
app.post('/api/template/addTemplate', templateroute.addTemplates);
app.post('/api/template/getAuthToken', upload.none(), templateroute.getAuthToken);
app.post('/api/template/generatePDF/:templateId', upload.fields(
  [
    {
      name: 'mainImage', maxCount: 1
    },
    {
      name: 'otherImage', maxCount: 1
    },
    {
      name: 'signature', maxCount: 1
    }
  ]
), templateroute.generatePDF);
app.delete('/api/template/removeTemplateById/:templateId', upload.none(), templateroute.removeTemplateById);
app.patch('/api/template/updateTemplateById/:templateId', upload.none(), templateroute.updateTemplateById);


/*--------------------------- API Guide------------------------------*/
app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});


//Connection with Database
var databaseURL = database.localdburl;
var mongoOpt = { useNewUrlParser: true };
mongoose.connect(databaseURL, mongoOpt);

http.createServer(app).listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
}); 