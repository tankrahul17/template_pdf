var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Collection template
var template = new Schema({
	date: String,
	title: String,
	subTitle: String,
	detail: String
}, 
{
	timestamps: true
}, 
{ collection: 'template' });

exports.template = mongoose.model('template', template); 