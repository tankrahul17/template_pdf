
/*-------------------------------------------------------*/
exports.isUndefinedOrNull = _isUndefinedOrNull;
exports.isValidId = _isValidId;
exports.chkDateInWeekDay = _chkDateInWeekDay;
exports.getFormatedDate = _getFormatedDate;
exports.formatyyyyMMDDTHHmmss = _formatyyyyMMDDTHHmmss;
/*-------------------------------------------------------*/
var id_regex = new RegExp("^[0-9a-fA-F]{24}$");

/*
TYPE:GET
TODO: To value is undefined or null.
*/
function _isUndefinedOrNull(value) {
	return (typeof value == 'undefined' || value == null || value == "")
}


/*
TYPE:GET
TODO: To check id is valid or not.
*/
function _isValidId(id) {
	if (typeof id != 'undefined' && id != "") {
		return id_regex.test(id);
	} else {
		return false;
	}
}

/*
TYPE:GET
TODO: To check id is valid or not.
*/
function _chkDateInWeekDay(date) {
	var d = new Date(date);
	if (d.getDay() == 6 || d.getDay() == 0) {
		return false
	} else if (9 <= d.getHours() && d.getHours() <= 17) {
		return true
	} else {
		return false
	}
}

/*
TYPE:GET
TODO: To formate Date and time
*/
function _formatyyyyMMDDTHHmmss(date) {
	var d = new Date(date);
	return d.getFullYear() + (((d.getMonth() + 1) > 9) ? (d.getMonth() + 1) : '0' + (d.getMonth() + 1)) + ((d.getDate() > 9) ? d.getDate() : '0' + d.getDate()) + 'T' + ((d.getHours() > 9) ? d.getHours() : '0' + d.getHours()) + ((d.getMinutes() > 9) ? d.getMinutes() : '0' + d.getMinutes()) + ((d.getSeconds() > 9) ? d.getSeconds() + 1 : '0' + d.getSeconds());
}

/*
TYPE:GET
TODO: To formate Date
*/
function _getFormatedDate(date) {
	var d = new Date(date);
	return d.getFullYear() + (((d.getMonth() + 1) > 9) ? (d.getMonth() + 1) : '0' + (d.getMonth() + 1)) + ((d.getDate() > 9) ? d.getDate() : '0' + d.getDate());
}
