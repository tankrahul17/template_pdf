var json = {};
var jwt = require('jsonwebtoken');
var CONSTANT = require('../config/constant.json');
var COMMON_SERVICES = require('./commonroute');
const fs = require('fs')
const path = require('path')
const utils = require('util')
const puppeteer = require('puppeteer')
const hb = require('handlebars')
const readFile = utils.promisify(fs.readFile)

var model = require('../models/model');
var TEMPLATE_COLLECTION = model.template;

var ObjectID = require('mongodb').ObjectID;
/*-------------------------------------------------------*/
exports.getAuthToken = _getAuthToken;
exports.getTemplates = _getTemplates;
exports.addTemplates = _addTemplates;
exports.generatePDF = _generatePDF;
exports.removeTemplateById = _removeTemplateById;
exports.updateTemplateById = _updateTemplateById;

/*-------------------------------------------------------*/

/*
o Get Template Token
*/
function _getAuthToken(req, res, next) {
    var email = req.body.email;
    var Template = { "email": email };

    var token = jwt.sign(Template, CONSTANT.superSecret, {
        expiresIn: 86400 // expires in 24 hours
    });
    json.status = '1';
    json.result = { "template": Template, "token": 'Basic ' + token };
    res.send(json);
}

/**
 * : Get templates
 * @param name Templatename. 
*/

function _getTemplates(req, res, next) {
    var json = {};
    var query = {};
    TEMPLATE_COLLECTION.find(query, function (templateerror, templates) {
        if (templateerror || templates.length <= 0) {
            json.status = '0';
            json.result = { 'Message': 'Template Not Found' };
            res.send(json);
        } else {
            json.status = '1';
            json.result = templates;
            res.send(json);
        }
    });
}

/**
 * Add templates 
*/

function _addTemplates(req, res, next) {
    var json = {};
    var date = req.body.date;
    var title = req.body.title;
    var subTitle = req.body.subTitle;
    var detail = req.body.detail;

    var template = new TEMPLATE_COLLECTION({
        date: date,
        title: title,
        subTitle: subTitle,
        detail: detail
    });

    var query = { title: title };
    TEMPLATE_COLLECTION.find(query, function (templateerror, getTemplate) {
        if (templateerror || getTemplate.length > 0) {
            json.status = '0';
            json.result = { 'Message': 'Template Already Exists' };
            res.send(json);
        } else {
            template.save(function (error, template) {
                if (error) {
                    json.status = '0';
                    json.result = { 'error': 'Error In Adding New Template' };
                    res.send(json);
                } else {
                    json.status = '1';
                    json.result = { 'Message': 'Template added successfully!' };
                    res.send(json);
                }
            });
        }
    });
}

/**
 * Generate PDF for template 
*/

async function _generatePDF(req, res, next) {
    var json = {};
    var template_id = req.params.templateId;
    if (!template_id) {
        json.status = '0';
        json.result = { 'Message': 'Template id require.' };
        res.send(json);
    }
    var query = { _id: new ObjectID(template_id) };
    TEMPLATE_COLLECTION.find(query, async function (templateerror, getTemplate) {
        if (templateerror || getTemplate.length <= 0) {
            json.status = '0';
            json.result = { 'Message': 'Template not found' };
            res.send(json);
        } else {
            // getTemplate = getTemplate[0]
            // await pdfFileGenerate(getTemplate, (fileName) => { 
            await pdfFileGenerate(req.body, (fileName) => {
                json.status = '1';
                json.result = { 'Message': 'PDF Generated successfully!', pdf: fileName };
                res.send(json)
            });
        }
    });
}

async function getTemplateHtml() {
    try {
        const tmpPath = path.resolve("./templates/template1.html");
        return await readFile(tmpPath, 'utf8');
    } catch (err) {
        return Promise.reject("Could not load html template");
    }
}

/*
 pdf File Generate
*/

async function pdfFileGenerate(data, callback) {
    getTemplateHtml().then(async (res) => {
        const template = hb.compile(res, { strict: true });
        var root = path.dirname(require.main.filename)

        // data.mainImage = fs.readFileSync(path.join(root, 'uploads', data.mainImage), 'base64'); 
        // data.otherImage = fs.readFileSync(path.join(root, 'uploads', data.otherImage), 'base64');
        // data.signature = fs.readFileSync(path.join(root, 'uploads', data.signature), 'base64');
        const result = template(data);
        const html = result;
        const browser = await puppeteer.launch();
        const page = await browser.newPage()
        await page.setContent(html)
        const fileName = path.join(root, 'pdfTemplates', new Date().getTime() + Math.floor(1000 + Math.random() * 9000) + '.pdf')
        await page.pdf({
            path: fileName,
            format: 'A4',
            margin: {
                top: 10,
                right: 10,
                bottom: 10,
                left: 10
            }
        })
        await browser.close();
        callback(fileName)
    }).catch(err => {
        console.error(err)
    });
}

/*
 To Remove Template By Id
*/
function _removeTemplateById(req, res, next) {
    var template_id = req.params.templateId;
    if (!template_id) {
        json.status = '0';
        json.result = { 'Message': 'Template id require.' };
        res.send(json);
    }
    TEMPLATE_COLLECTION.deleteOne({ _id: new ObjectID(template_id) }, function (error, result) {
        if (error) {
            json.status = '0';
            json.result = { 'error': 'Error In Removing New Template' };
            res.send(json);
        } else {
            json.status = '1';
            json.result = { 'Message': 'Template removed successfully!' };
            res.send(json);
        }
    });
}

/*
 To Update Template By Id
*/
function _updateTemplateById(req, res, next) {
    var template_id = req.params.templateId;
    var date = req.body.date;
    var title = req.body.title;
    var subTitle = req.body.subTitle;
    var detail = req.body.detail;

    if (!template_id) {
        json.status = '0';
        json.result = { 'Message': 'Template id require.' };
        res.send(json);
    }

    var query = {
        $set: {
            date: date,
            title: title,
            subTitle: subTitle,
            detail: detail
        }
    };


    TEMPLATE_COLLECTION.find({ title: title }, function (templateerror, getTemplate) {
        if (templateerror || (getTemplate.length > 0 && getTemplate[0]._id != template_id)) {
            json.status = '0';
            json.result = { 'Message': 'Template Already Exists' };
            res.send(json);
        } else {
        TEMPLATE_COLLECTION.updateOne({ _id: new ObjectID(template_id) }, query, function (error, result) {
            if (error) {
                json.status = '0';
                json.result = { 'error': 'Error In Updating New Template' };
                res.send(json);
            } else {
                json.status = '1';
                json.result = { 'Message': 'Template updated successfully!' };
                res.send(json);
            }
        });
        }
    });
}

